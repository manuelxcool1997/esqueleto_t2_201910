package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {


    void add( T item);
	
	void addAtEnd(T item);
	
	void addAt(int pos, T item) throws Exception;
	
	void delete(T item);
	
	void deleteAt(int pos) throws Exception;
	
	T getElement();
	
	T getCurrentElement();

	Integer getSize();
	
	T next();
	
	T previous();


}
