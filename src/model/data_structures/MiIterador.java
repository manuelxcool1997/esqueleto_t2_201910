package model.data_structures;

import java.util.Iterator;

public class MiIterador<T> implements Iterator<T> 
{
	Node<T> actual;
	
	public  MiIterador(Node<T> primer) 
	{
		this.actual = primer;
	}
	
	@Override
	public boolean hasNext() 
	{
		return (actual != null);
	}

	@Override
	public T next() 
	{
		T siguiente = actual.darElemento();
		actual = actual.darSiguiente();
		return siguiente;
	}

}
