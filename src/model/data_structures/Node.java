package model.data_structures;

public class Node <T>
{
private T elemento;
	
	private Node<T> anterior;
	
	private Node<T> siguiente;

	public Node(T pElemento) 
	{
		elemento = pElemento;
	}
	
	public T darElemento() 
	{
		return elemento;
	}

	public Node<T> darAnterior()
	{
		return anterior;
	}

	public void cambiarAnterior(Node<T> anterior) 
	{
		this.anterior = anterior;
	}

	public Node<T> darSiguiente()
	{
		return siguiente;
	}

	public void cambiarSiguiente(Node<T> siguiente)
	{
		this.siguiente = siguiente;
	}

	public boolean tieneSiguiente() 
	{
		return siguiente != null;
	}

	public void asignarNuevoElemento(T nuevoElemento) 
	{
		elemento = nuevoElemento;
	}
	

}
