package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T> {
private Node<T> primero;
	
	private Node<T> ultimo;
	
	private Node<T> actual;
	
	private int tamanoActual;

	@Override
	public void add(T item) 
	{
		if (primero == null) 
		{
			primero = new Node<T>(item);
		} 
		else 
		{
			actual = primero;
			cambiarSiguiente(item);
			if(!actual.tieneSiguiente())
				ultimo =  actual;
		}
		tamanoActual++;
	}

	@Override
	public void addAtEnd(T item) 
	{
		if (primero == null)
		{
			primero = new Node<T>(item);
		}
		else 
		{
			if (!primero.tieneSiguiente()) 
			{
				primero.cambiarSiguiente(new Node<T>(item));
			} 
			else 
			{
				actual = primero;
				while (actual.tieneSiguiente() && actual!=null) 
				{
					Node<T> siguiente = actual.darSiguiente();
					if (ultimo == actual)
					{
						actual.cambiarSiguiente(new Node<T>(item));
						ultimo = actual;
					}
					actual = siguiente;
				}
			}
		}
		tamanoActual++;
	}

	@Override
	public void delete(T item) 
	{
		if (primero != null) 
		{
			if(!primero.tieneSiguiente() && primero.darSiguiente().darElemento().hashCode() == item.hashCode())
			{
				primero = null;
				tamanoActual--;
			}
			else
			{
				while (actual.tieneSiguiente() && actual!=null) 
				{
					Node<T> siguiente = actual.darSiguiente();
					if (actual.darElemento().hashCode() == item.hashCode())
					{
						eliminarElementoActualDeLista();
					}
					actual = siguiente;
				}
			}
		} 
		
	}

	@Override
	public void deleteAt(int pos) throws Exception 
	{
		int contador = 0;
		if (primero != null) 
		{
			if(!primero.tieneSiguiente())
			{
					if (pos == 0)
					{
						primero = null;
						tamanoActual--;
					}
					else
					{
					  throw new Exception("No es posible eliminar el elemento, ya que no hay ningun elemneto en la posicion: "+pos);
					}
			}
			else
			{
				while (actual.tieneSiguiente() && actual!=null) 
				{
					contador++;
					Node<T> siguiente = actual.darSiguiente();
					if (contador == pos)
					{
						eliminarElementoActualDeLista();
						tamanoActual--;
					}
					actual = siguiente;
				}
			}
		}	
	}

	@Override
	public Integer getSize() 
	{
		return tamanoActual;
	}

	@Override
	public T next() 
	{
		return actual.darSiguiente().darElemento();
	}

	@Override
	public T previous() 
	{
		return actual.darAnterior().darElemento();
	}

	@Override
	public void addAt(int pos, T item) 
	{
		if (pos < tamanoActual && pos > 0) 
		{
			if (pos == 0)
			{
				actual = primero;
				cambiarSiguiente(item);
			} 
			else if (pos == tamanoActual) 
			{
				addAtEnd(item);
			} 
			else 
			{
				int contador = 0;
				actual = primero;
				while (actual.tieneSiguiente())
				{
					contador++;
					if (contador - 1 == pos)
						cambiarSiguiente(item);
					actual = primero.darSiguiente();
				}
			}
		}
	}

	

	@Override
	public T getElement() 
	{
		//TODO 
		return primero.darElemento();
	}

	@Override
	public T getCurrentElement() 
	{
		return actual.darElemento();
	}

	private void cambiarSiguiente(T item) 
	{ 
		Node<T> nuevoSiguiente = new Node<T>(item);
		nuevoSiguiente.cambiarSiguiente(actual);
		actual.cambiarAnterior(nuevoSiguiente);
		primero = nuevoSiguiente;
	}

	private void eliminarElementoActualDeLista() 
	{
		Node<T> anterior = actual.darAnterior();
		Node<T> siguiente = actual.darSiguiente();
		anterior.cambiarSiguiente(siguiente);
		siguiente.cambiarAnterior(anterior);
		actual.cambiarSiguiente(null);
		actual.cambiarAnterior(null);
		actual = siguiente;
	}

	@Override
	public Iterator<T> iterator()
	{
		return new MiIterador<>(primero);
	}



}
