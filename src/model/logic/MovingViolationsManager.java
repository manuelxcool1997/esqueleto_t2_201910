package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.sun.corba.se.impl.orbutil.graph.Node;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.vo.VOTrip;
import model.data_structures.LinkedList;
import model.data_structures.QueueTrip;
import model.data_structures.StackTrip;

public class MovingViolationsManager implements IMovingViolationsManager {

	public LinkedList<VOMovingViolations> lista=new LinkedList<VOMovingViolations>();
	
	public void loadMovingViolations(String movingViolationsFile){
		File archivo = new File(movingViolationsFile);
		
		if(archivo != null)
		{
			try 
			{
				BufferedReader reader = new BufferedReader(new FileReader(moving));
				while (reader.ready())
				{
					
					String[] linea=reader.readLine().split(",,");
					VOMovingViolations object= new VOMovingViolations();
					object.changeObjectId(Integer.parseInt(linea[0]));
					object.changeLocation(linea[2]);
					object.changeAccidentIndicator(linea[12]);
					object.changeTicketssueData(linea[14]);
					object.changeTotalpaid(Integer.parseInt(linea[9]));
					object.changeViolenDescription(linea[16]);
					
					lista.add(object);
					
					
					
				}
				lista.deleteAt(0);;
				reader.close();

			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		
	}

		
	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		LinkedList<VOMovingViolations> listaCodigo= new LinkedList<VOMovingViolations>();
		
		while(lista.iterator().hasNext())
		{
			if(lista.getCurrentElement().getAccidentIndicator().equals(violationCode))
				listaCodigo.add(lista.getCurrentElement());
		}
		
		return listaCodigo;
		
	}

	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		
LinkedList<VOMovingViolations> listaCodigo= new LinkedList<VOMovingViolations>();
		
		while(lista.iterator().hasNext())
		{
			if(lista.getCurrentElement().getViolationDescription().equals(accidentIndicator))
				listaCodigo.add(lista.getCurrentElement());
		}
		
		return listaCodigo;
	}	


}
