package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	public int objectID;
	public String Location;
	public String TIcketIssueDate;
	public int TotalPaid;
	public String accidentIndicator;
	public String ViolationDescription;
	
	
	
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void changeObjectId(int pID)
	{
		objectID=pID;
	}
	
	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return "";
	}
	
	public void changeLocation(String pLocation)
	{
		Location=pLocation;
	}
	

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return "";
	}
	
	public void changeTicketssueData(String pTicketIssueDate)

	{
	TIcketIssueDate=pTicketIssueDate;	
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void changeTotalpaid(int ptotalpaid)
	{
		TotalPaid=ptotalpaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return "";
	}
	
	public void changeAccidentIndicator(String pAccidentIndicator)
	{
		accidentIndicator=pAccidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return "";
	}
	
	public void changeViolenDescription(String pViolenDescription)
	{
		ViolationDescription=pViolenDescription;
	}
}
